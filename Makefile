CC = gcc
CFLAGS = -Wall -Wextra -O0
LDFLAGS = -Wl,-Tscript.ld
SOURCES=$(wildcard src/*.c)
OBJECTS=$(addprefix obj/,$(notdir $(SOURCES:.c=.o)))

all: mallogc

debug: CFLAGS += -DDEBUG -g
debug: all

mallogc: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $+

obj/%.o: src/%.c | obj
	$(CC) $(CFLAGS) -c -o $@ $<

obj:
	@mkdir -p $@

clean:
	rm -rf obj mallogc
