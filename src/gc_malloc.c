#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>

#include "gc_malloc.h"

typedef struct meta_t {
	size_t size;
	unsigned flags;
	struct meta_t *next;
	struct meta_t *prev;
} __attribute__((packed, aligned(8))) meta_t;

#define MIN_ALLOC_SIZE 4096
#define META_SIZE      (sizeof(meta_t))
#define ALIGN_8(x)     ((x % 8) ? ((x + 8) & ~0x7) : x)

#define IS_MARKED(x)   (x->flags & 0x2)
#define MARK_GC(x)     (x->flags |= 0x2)
#define UNMARK_GC(x)   (x->flags &= ~0x2)

#define IS_FREE(x)     (x->flags & 0x1)
#define MARK_FREE(x)   (x->flags |= 0x1)
#define UNMARK_FREE(x) (x->flags &= ~0x1)

static meta_t *base = NULL;

/* allocate at least one page (4KB) of memory per sbrk call 
 * and split it into smaller chunks. 
 * This way we don't have to use system calls all the time */
static meta_t *sys_alloc_mem(size_t size)
{
	meta_t *b = NULL;
	size_t rsize = (size < MIN_ALLOC_SIZE) ? MIN_ALLOC_SIZE : size;

	if ((b = sbrk(rsize)) != (void*)-1) {
		UNMARK_FREE(b);
		b->size = rsize;
		b->prev = b->next = NULL;
	} else {
		fprintf(stderr, "[ERROR] sbrk failed\n");
		b = NULL;
	}
	return b;
}

/* split block b into two smaller blocks sizes of split and b->size - split 
 * return what's left after the split if the split is sane, otherwise return NULL */
static meta_t *split_block(meta_t *b, size_t split)
{
	if (b->size <= split || b->size - split - META_SIZE <= 0)
		return NULL;

	meta_t *tmp = (meta_t*)((char*)b + split);
	MARK_FREE(tmp);
	tmp->size = b->size - split;
	b->size = split;

	return tmp;
}

/* search through the memory region and look for references to 
 * memory blocks allocated from heap. If a reference to a block is
 * found, mark it. Otherwise do nothing. */
/* TODO: explain what counts as a reference */
static void mark_memory_region(uint64_t *start, uint64_t *end)
{
	for (; start < end; ++start) {
		uint64_t v = *start;
		meta_t *b = base;

		do {
			uint64_t bstart = (uint64_t)((meta_t*)b + 1);
			uint64_t bend   = (uint64_t)((char*)b + b->size);

			if ((v >= bstart && v <= bend) || IS_FREE(b)) {
				MARK_GC(b); 
			}
		} while ((b = b->next) != NULL);
	}
}

/* read stack start from /proc/self/stat (sorry windows users)
 * stack start is the 29th field */
static uint64_t find_stack_start(void)
{
	char buffer[1024], *t, *p;
	FILE *fp = fopen("/proc/self/stat", "r");

	fread(buffer, 1024, 1, fp);
	fclose(fp);

	p = buffer;
	for (int i = 0; i < 28; ++i) {
		t = strtok_r(p, " ", &p);
	}

	return (uint64_t)atol(t);
}

/* data/bss segments, stack and heap can contain references to our memory blocks. 
 * search through all those memory segments all  
 *
 * Use UINT64_MAX as a sentinel value so we don't call 
 * find_stack_start multiple times (the value doesn't change) */
void mark_and_sweep(void)
{
#ifdef DEBUG
	puts("------- start of mark_and_sweep -------");
#endif

	extern uint64_t __data_start, _end; /* thanks mr. linker */
	uint64_t stack_end;
	static uint64_t stack_start = UINT64_MAX;
	meta_t *b, *tmp = NULL;

	/* find beginning and end of stack */
	asm ("mov %%rbp, %0" : "=r" (stack_end));
	if (stack_start == UINT64_MAX)
		stack_start = find_stack_start();

	/* mark... */
	mark_memory_region((uint64_t*)&__data_start, (uint64_t*)&_end);
	mark_memory_region((uint64_t*)stack_end, (uint64_t*)stack_start);

	b = base;
	for (int bcnt = 0; b != NULL; bcnt++, b = b->next) {
		if (!IS_FREE(b)) {
			uint64_t *start = (uint64_t*)b + 1;
			uint64_t *end = (uint64_t*)((char*)b + b->size);
			uint64_t v, block_start, block_end;

			for (; start < end; ++start) {
				v = *start;
				tmp = base;

				for (int tmpcnt = 0; tmp != NULL; tmp = tmp->next) {
					if (bcnt > tmpcnt && !IS_MARKED(b))
						continue;

					block_start = (uint64_t)((meta_t*)tmp + 1);
					block_end = (uint64_t)((char*)tmp + tmp->size);

					if ((v >= block_start && v <= block_end) || IS_FREE(tmp)) {
						MARK_GC(tmp); 
					}
				}
			}
		}
	}

	/* ... and sweep */
	b = base;
	while (b) {
		if (IS_MARKED(b)) {
#ifdef DEBUG
			printf("block %p of size %zu is NOT trash\n", b, b->size);
#endif
			UNMARK_GC(b);
		} else {
#ifdef DEBUG
			printf("block %p of size %zu IS trash\n", b, b->size);
#endif
			MARK_FREE(b);
		}
		b = b->next;
	}

#ifdef DEBUG
	puts("------- end of mark_and_sweep -------\n\n");
#endif


	b = base;
	while (b) {
		if (!IS_FREE(b)) { /* move to next block */
			b = b->next;
			continue;
		}

		/* merge all adjacent free blocks */
		while (b->next && IS_FREE(b->next)) {
			b->size += b->next->size;
			b->next = b->next->next;
			if (b->next)
				b->next->prev = b;
		}

		tmp = b->next;
		if ((b->size >= MIN_ALLOC_SIZE) && 
			(sbrk(0) == (char*)b + b->size))
		{
			if (b->prev) b->prev->next = b->next;
			if (b->next) b->next->prev = b->prev;
			if (base == b) base = b->next;
			brk(b);
		}

		b = tmp;
	}
}

static meta_t *find_free_block(size_t size)
{
	meta_t *b = base, *tmp = NULL;

	while (b && !(IS_FREE(b) && b->size >= size)) {
		b = b->next;
	}

	if (b != NULL) {
		UNMARK_GC(b);
		UNMARK_FREE(b);

		if ((tmp = split_block(b, size)) != NULL) {
			tmp->next = b->next;
			if (b->next)
				b->next->prev = tmp;
			tmp->prev = b;
			b->next = tmp;
		}
	}
	return b;
}

void *gc_new(size_t size)
{
	meta_t *b = NULL, *tmp = NULL;
	size_t req_size = ALIGN_8(size) + META_SIZE;

	if ((b = find_free_block(req_size)) == NULL) {
		if (base != NULL)
			mark_and_sweep();

		if ((b = find_free_block(req_size)) == NULL) {
			if ((b = sys_alloc_mem(req_size)) == NULL)
				return NULL;

			if ((tmp = split_block(b, req_size)) != NULL) {
				if (base == NULL) {
					base = tmp->prev = b;
					b->next = tmp;
					tmp->next = b->prev = NULL;
				} else {
					tmp->next = base;
					base->prev = b->next = tmp;
					tmp->prev = base = b;
					b->prev = NULL;
				}
			} else {
				if (base != NULL) {
					base->prev = b;
					b->next = base;
				}
				base = b;
			}
		}
	}

	return b + 1;
}
