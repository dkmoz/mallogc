#ifndef __gcmalloch__
#define __gcmalloch__

#include <stddef.h> /* size_t */

void *gc_new(size_t size);

#ifdef DEBUG
void mark_and_sweep(void);
#endif

#endif /* end of include guard: __gcmalloch__ */
