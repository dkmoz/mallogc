#include "gc_malloc.h"

/* data segment test (initialized) (static variable defined in test_data_segment) */
int *ds_global = (int*)0x12345;

/* bss segment test (uninitialized) (static variable defined in test_bss_segment) */
int *bss_global;

/* 72 bytes */
int **test_reachable(void)
{
	int *tmp = gc_new(sizeof(int) * 10);   /* reachable */
	int **tmp_ptr = gc_new(sizeof(int*) * 5); /* reachable */

	*tmp_ptr = tmp;
	return tmp_ptr;
}

/* 112 bytes */
int **test_unreachable(void)
{
	int *tmp = gc_new(sizeof(int) * 20);       /* unreachable */
	int **tmp_ptr = gc_new(sizeof(int*) * 10); /* unreachable */

	*tmp_ptr = tmp;

	return NULL;
}

/* 152 bytes */
void test_data_segment(void)
{
	static int **ds_static = &ds_global;

	int *tmp = gc_new(sizeof(int) * 30);       /* reachable */
	int **tmp_ptr = gc_new(sizeof(int*) * 15); /* reachable */

	ds_global = tmp;
	ds_static = tmp_ptr;
}

/* 192 bytes */
void test_bss_segment(void)
{
	static int **bss_static;

	int *tmp = gc_new(sizeof(int) * 40);       /* reachable */
	int **tmp_ptr = gc_new(sizeof(int*) * 20); /* reachable */

	bss_global = tmp;
	bss_static = tmp_ptr;
}


int main(void)
{
	char *tmp         = gc_new(10294); /* reachable */
	int **reachable   = test_reachable(); /* reachable */
	int **unreachable = test_unreachable(); /* unreachable (as it should be) */
	test_data_segment(); /* reachable */
	test_bss_segment(); /* reachable */

#ifdef DEBUG
	mark_and_sweep();
#endif
}
