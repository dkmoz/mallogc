# mallogc

## About
Mallogc is a garbage-collecting memory allocator written in C. It uses the [mark-and-sweep method](https://en.wikipedia.org/wiki/Tracing_garbage_collection#Na.C3.AFve_mark-and-sweep) to provide automatic memory deallocation.


The memory allocator I've written in very naïve and inefficient so don't use it in your projects (I won't either). The point of this project was to learn about memory allocation and garbage collection, not to write the most efficient allocator on planet.


Apparently data segment doesn't always follow text segment. This was something I wasn't aware of before this project but thank god ld supports scripting. I was able to define `__data_start` with script.lds and didn't have to parse ELF headers. Whew.

## How it works

How the memory allocator works:
1. user calls `gc_new`
2. `gc_new` calls `find_free_block`
   * Free block big enough found, return it
   * Free block 404, call `mark_and_sweep`
       * Mark all referenced memory blocks
       * Sweep all unmarked objects
         * Mark block as free
		 * Merge adjacent free blocks
		 * Release memory back to OS if possible
3. Call `find_free_block` again
    * Free block big enough found, return it
    * Free block still 404, call `sys_alloc_mem`
        * Allocate 4096 bytes (one page) of memory and split it into smaller chunks and return a chunk of memory to user



The interface is very simple. All you need to do is call `gc_new(<size in bytes>)` and you're returned a pointer to the memory block. Whenever the last reference to the block is lost (out of scope (muh stackframes), setting pointer to NULL etc.), the collector will detect this and sweep the object automatically. 

## Copying
mallogc is free software. It's licensed under the MIT license
